# Install dataset

Run the script and it will download and preprocess the movielens dataset. Two directories will be created: movielens-raw-csv and movielens-preprocessed.
```
./download_and_preprocess.sh
```

# Parsing details

## Users
Parsed out from ratings data.

## Products
Parsed out from movie data.

## Categories
Parsed out from movie data.

## Transactions
Parsed out from ratings, any rating of 4.0, 4.5 or 5.0 is considered to be a transaction, all lower ratings are discarded.

