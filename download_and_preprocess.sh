#!/bin/bash

#################################################################
# Remove old data dirs if needed 
if [ -d movielens-raw-csv ] ; then
  echo "Removing old movielens-raw-csv dir"
  rm -rf movielens-raw-csv
fi

#################################################################
# Download and unpack dataset
echo "Downloading dataset..."
curl -O  http://files.grouplens.org/datasets/movielens/ml-20m.zip

echo "Unpacking..."
unzip ml-20m.zip
rm ml-20m.zip
mv ml-20m movielens-raw-csv 

#################################################################
# Create preprocess folder 
mkdir csv

#################################################################
# Generate users header
echo "Generating users.csv"
echo "id,name" > csv/users.csv 

# Generate users
tail -n +2 movielens-raw-csv/ratings.csv | sed 's|,| |g' | awk '{ print $1",\"Jane Doe\"" }' | uniq >> csv/users.csv

#################################################################
# Generate products header
echo "Generating products.csv"
echo "id,name,categories" > csv/products.csv
tail -n +2 movielens-raw-csv/movies.csv  >> csv/products.csv


#################################################################
# Generate categories header
echo "Generating categories.csv"
echo "id,product_id,name" > csv/categories.csv

# Generate categories
tail -n +2 movielens-raw-csv/movies.csv | \
sed 's|,| |g' | \
awk '{ print $1" "$NF }' | \
sed 's|\|| |g' | \
awk '{ print $1","$2"\n"$1","$3"\n"$1","$4"\n"$1","$5"\n"$1","$6"\n"$1","$7"\n"$1","$8"\n"$1","$9"\n"$1","$10"\n"$1","$11"" }' | \
sed 's|,| |g' | \
awk '{ if ( NF != 1 ) print $0 }' | \
awk '{ print NR","$1","$2}' \
>> csv/categories.csv

#################################################################
# Generate transaction header
echo "Generating transactions.csv"
echo "id,user_id,product_id,ts" > csv/transactions.csv 

# Generate transaction from ratings by considering any rating of 4-5 as a transaction. Also unique transaction ids.
cat movielens-raw-csv/ratings.csv | grep -e ",4\.0," -e ",4\.5," -e ",5\.0," | sed 's|,| |g' | awk '{ print NR","$1","$2","$4}' >> csv/transactions.csv

