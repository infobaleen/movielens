#!/bin/bash -e

HOST=${1:?"Specify host as first argument"}
API=${HOST}/api/v1
KEY=${2:?"Specify API key as second argument"}
CURL="curl -sS --fail"
AUTH="-H Authorization:${KEY}"
JSON="-H Content-Type:application/json"

echo Creating sources...
USER=`${CURL} -X POST "${API}/sources" ${AUTH} ${JSON} -d '{"Name":"users"}' | jq -r .Id`
echo USER source: ${USER}
ITEM=`${CURL} -X POST "${API}/sources" ${AUTH} ${JSON} -d '{"Name":"items"}' | jq -r .Id`
echo ITEM source: ${ITEM}
INTERACTION=`${CURL} -X POST "${API}/sources" ${AUTH} -d '{"Name":"interactions"}' | jq -r .Id`
echo INTERACTION source: ${INTERACTION}
TAGS=`${CURL} -X POST "${API}/sources" ${AUTH} -d '{"Name":"tags"}' | jq -r .Id`
echo TAGS_MOVIE source: ${TAGS}

echo Uploading users...
${CURL} -X POST "${API}/sources/${USER}/id/upload-csv" ${AUTH} -F file=@users.csv
echo
echo Uploading movies...
${CURL} -X POST "${API}/sources/${ITEM}/id/upload-csv" ${AUTH} -F file=@movies.csv
echo
echo Uploading interactions...
${CURL} -X POST "${API}/sources/${INTERACTION}/id/upload-csv" ${AUTH} -F file=@interactions.csv
echo
echo Uploading tags...
${CURL} -X POST "${API}/sources/${TAGS}/id/upload-csv" ${AUTH} -F file=@tags.csv
echo

echo Creating buckets...
USER=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"users", "ProcessingEnabled":true, "Fields":[{"Type":"string", "Identifier":"name", "SourceId":'${USER}', "IdentifierInSource":"default_name"}]}' | jq -r .Id`
echo USER bucket: ${USER}
ITEM=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"items", "ProcessingEnabled":true, "Fields":[{"Type":"string", "Identifier":"title", "SourceId":'${ITEM}', "IdentifierInSource":"default_name"}]}' | jq -r .Id`
echo ITEM bucket: ${ITEM}
INTERACTION=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"interactions", "ProcessingEnabled":true, "Fields":[{"Type":"epoch", "Identifier":"timestamp", "SourceId":'${INTERACTION}', "IdentifierInSource":"default_ts"},{"Type":"int64", "Identifier":"user", "SourceId":'${INTERACTION}', "IdentifierInSource":"default_user_id"},{"Type":"int64", "Identifier":"item", "SourceId":'${INTERACTION}', "IdentifierInSource":"default_product_id"}]}' | jq -r .Id`
echo INTERACTION bucket: ${INTERACTION}
TAGS=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"tags", "ProcessingEnabled":true, "Fields":[{"Type":"string", "Identifier":"name", "SourceId":'${TAGS}', "IdentifierInSource":"default_name"},{"Type":"int64", "Identifier":"item", "SourceId":'${TAGS}', "IdentifierInSource":"default_product_id"}]}' | jq -r .Id`
echo TAGS bucket: ${TAGS}


echo Creating data model...
BODY='{"Name":"data_model", "Root":{"Node":{"Bucket": '${INTERACTION}',"ChildValues":[{"FieldName":"user","Node":{"Bucket":'${USER}'}},{"FieldName":"item","Node":{"Bucket":'${ITEM}'}}]},"UserField":"user","ItemField":"item","TimestampField":"timestamp"}}'
echo $BODY

DATAMODEL=`${CURL} -X POST "${API}/data-models" ${AUTH} -d "$BODY" | jq -r .Id`
echo Data model: ${DATAMODEL}



